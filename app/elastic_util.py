from elasticsearch import Elasticsearch
import os


# connect to elastice search server
def connect_elasticsearch():
    ES_HOST = os.environ.get('ES_HOST') or 'localhost'
    ES_PORT = os.environ.get('ES_PORT') or 9200
    es = None
    es = Elasticsearch([{'host': ES_HOST, 'port': ES_PORT}])
    if es.ping():
        print(' * Elastic Search is connected!')
    else:
        print(' * Elastic Search is not connected!')
    return es


# create clients index
def create_index_clients(es_object):
    index_name = 'clients'
    created = False
    # index settings
    settings = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        },
        "mappings": {
            "members": {
                "dynamic": "strict",
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "name": {
                        "type": "text"
                    }
                }
            }
        }
    }
    try:
        if not es_object.indices.exists(index_name):
            # Ignore 400 means to ignore "Index Already Exist" error.
            es_object.indices.create(index=index_name, ignore=400,
                                     body=settings)
            print('Created Index')
        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created


def store_record_client(elastic_object, record):
    index_name = 'clients'
    try:
        outcome = elastic_object.index(index=index_name, doc_type='members',
                                       body=record)
        print(" * Elasticesearch : One record is stored!")
        print(record)
    except Exception as ex:
        print('Error in storing data')
        print(str(ex))


def update_record_client(elastic_object, id, record):
    index_name = 'clients'
    try:
        search = {
            "query": {
                "match": {
                    "id": id
                }
            }
        }
        result = elastic_object.search(index='clients', body=search)
        print(result)
        meta_id = result['hits']['hits'][0]['_id']
        print(meta_id)
        elastic_object.update(index=index_name, doc_type='members',
                              id=meta_id, body=record)
        print(" * Elasticesearch : One record is updateded!")
        print(record)
    except Exception as ex:
        print('Error in updating data')
        print(str(ex))


def search_client_es(elastic_object, string):
    search = {
        "query": {
            "match_phrase_prefix": {
                "name": {
                    "query": string,
                }
            }
        }
    }
    result = elastic_object.search(index='clients', body=search)
    result = result['hits']['hits']
    datas = []
    for hit in result:
        datas.append(hit["_source"]["name"])

    datas = {'data': datas}
    return datas
