function update_progress(status_url) {
    // send GET request to status URL
    $.getJSON(status_url, function(data) {
        // update UI
        status = data['status'];
        current = data['current'];
        $('#status-bar').attr('aria-valuenow', current).css("width", current + "%").html(current + "%");
        if (data['state'] != 'PENDING' && data['state'] != 'PROGRESS') {
            if ('filepath' in data) {
                // show result
                console.log(data['filepath']);
                $("#status-div").html("<h3 class='text-success'>Exported Successfully.</h3>");
                download(data['filepath']);
                // $('#download-link').click();
            }
            else {
                // something unexpected happened
                document.getElementById("status-bar").classList.add("bg-danger");
                $('#status-bar').attr('aria-valuenow', 100).css("width", "100%").html("Something went wrong.");
                $("#status-div").html("<h3 class='text-danger'>Something went wrong.</h3>");
            }
            $("#start-export").html('Export');
            document.getElementById("start-export").disabled = false;
        }
        else {
            setTimeout(function() {
                update_progress(status_url);
            }, 1000);
        }
    });
}
$(function() {
    $('#start-export').click(start_task);
});
function download(filepath) {
    filename = filepath.split("/").pop();
    var pom = document.createElement('a');
    pom.setAttribute('href', filepath);
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}