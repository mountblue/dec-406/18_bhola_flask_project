function showHint() {
    let str = input_id.value
    let xmlhttp;
    if (str.length==0) {
        console.log("");
        document.getElementById('clients').innerHTML = "";
        return;
    }
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }else {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            console.log(xmlhttp.responseText);
            let clientsName = JSON.parse(xmlhttp.responseText)
            let options = "";
            for(let i=0; i<clientsName['data'].length; i++){
                options += '<option value="'+clientsName['data'][i]+'" />';
            }
            document.getElementById('clients').innerHTML = options;
        }
    }
    xmlhttp.open("GET","/search/clients?string="+str, true);
    xmlhttp.send();
}