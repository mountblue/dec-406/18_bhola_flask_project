from datetime import datetime
from app import app, db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, current_user
from sqlalchemy.sql import func


# Define the User data-model
class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    # Relationships
    roles = db.relationship('Role', secondary='user_roles')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __repr__(self):
        return '<Role {}>'.format(self.name)


# Define the UserRoles association table
class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer,
                        db.ForeignKey('roles.id', ondelete='CASCADE'))


# Define the Address data-model
class Address(db.Model):
    __tablename__ = 'addresses'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    address = db.relationship('Client', backref='address', lazy='dynamic')

    def __repr__(self):
        return '<Address {}>'.format(self.name)


# Define the Client data-model
class Client(db.Model):
    __tablename__ = 'clients'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    address_id = db.Column(db.Integer, db.ForeignKey('addresses.id'),
                           nullable=False)
    client = db.relationship('Record', backref='client', lazy='dynamic')
    clients = db.relationship('Address', backref='clients')

    def __repr__(self):
        return '<Client {} : {}>'.format(self.name, self.address_id)

    def get_amount(self):
        try:
            total_sale =\
                db.session.query(func.sum(Record.amount)
                                     .label('total_sale'))\
                .filter(Record.type_ == "Sale", Record.client_id == self.id)\
                .group_by(Record.client_id).first()[0]
        except Exception:
            total_sale = 0

        try:
            total_deposit =\
                db.session.query(func.sum(Record.amount)
                                     .label('total_deposit'))\
                .filter(Record.type_ == "Deposit",
                        Record.client_id == self.id)\
                .group_by(Record.client_id).first()[0]
        except Exception:
            total_deposit = 0

        amount = round(total_sale-total_deposit, 2)

        return amount

    @staticmethod
    def get_total_client():
        try:
            total_client = db.session.query(func.count(Client.id)).first()[0]
        except Exception:
            return 0
        return total_client


# Define the Record data-model
class Record(db.Model):
    __tablename__ = 'records'
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey('clients.id'),
                          nullable=False)
    type_ = db.Column(db.String(50))
    amount = db.Column(db.Float(precision=2))
    date_transaction = db.Column(db.DateTime, index=True,
                                 default=datetime.utcnow)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    last_updateda_at = db.Column(db.DateTime, index=True,
                                 default=datetime.utcnow)
    records = db.relationship('Client', backref='records')

    def __repr__(self):
        return '<Record {} : {} : {}>'.format(self.id, self.client,
                                              self.amount)

    @staticmethod
    def get_total_sale():
        try:
            total_sale =\
                db.session.query(func.sum(Record.amount))\
                  .filter(Record.type_ == "Sale").first()[0]
        except Exception:
            return 0
        return round(total_sale, 2)

    @staticmethod
    def get_total_deposit():
        try:
            total_deposit =\
                db.session.query(func.sum(Record.amount))\
                  .filter(Record.type_ == "Deposit").first()[0]
        except Exception:
            return 0
        return round(total_deposit, 2)


# User loader
@login.user_loader
def load_user(id):
    return User.query.get(int(id))
