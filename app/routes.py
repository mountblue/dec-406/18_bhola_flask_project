from flask import render_template, flash, redirect, url_for,\
    request, current_app, abort, jsonify
from app import app, db, celery, APP_ROOT
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Role, Client, Record, Address
from werkzeug.urls import url_parse
from app.forms import RegistrationForm, LoginForm, ClientEditForm,\
    AddressEditForm, AddressForm, ClientForm, RecordForm
from functools import wraps
import datetime
from sqlalchemy import desc
import json
from app.elastic_util import connect_elasticsearch,\
    search_client_es
from app.celery_util import export_client_task, add_record_elastic,\
    update_record_elastic


# Login and role required decorator
def login_role_required(role=[]):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
                return current_app.login_manager.unauthorized()
            urole = current_user.roles[0].name
            if urole not in role:
                abort(403)
                return
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


# Home page of the application
@app.route('/')
@app.route('/index')
@login_required
def index():
    total = Client.get_total_client()
    total_sale = Record.get_total_sale()
    total_deposit = Record.get_total_deposit()
    clients = Client.query.order_by('name').all()
    clients_amount = {}
    for client in clients:
        amount = client.get_amount()
        if amount == 0:
            continue
        if amount < 0:
            amount_list = []
            amount_list.append(amount)
            amount_list.append(0)
            pass
        elif amount > 0:
            amount_list = []
            amount_list.append(0)
            amount_list.append(amount)
            pass
        clients_amount[client.name] = amount_list
    return render_template("index.html", title='Home Page', total=total,
                           total_sale=total_sale, total_deposit=total_deposit,
                           clients_amount=clients_amount)


# Login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password', category='danger')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


# Logout the user
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


# Register new user to application
@app.route('/register', methods=['GET', 'POST'])
@login_role_required(role=['Admin'])
def register():
    form = RegistrationForm()
    form.role_id.choices =\
        [(r.id, r.name) for r in Role.query.order_by('name')]
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        role = Role.query.filter_by(id=form.role_id.data).first()
        user.roles = [role, ]
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you added a new user!', category='success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


# show all address
@app.route('/address')
def address():
    addresses = Address.query.order_by('name').all()
    return render_template('address/address.html', title='Locations',
                           addresses=addresses)


# Show form to add address
@app.route('/address/add', methods=['GET', 'POST'])
@login_role_required(role=['Admin'])
def address_add():
    form = AddressForm()
    if form.validate_on_submit():
        address = Address(name=form.name.data)
        db.session.add(address)
        db.session.commit()
        flash('You added a new address!', category='success')
        return redirect(url_for('address'))
    return render_template('address/address_add.html', title='Add Location',
                           form=form)


# Show form to edit address
@app.route('/address/<int:address_id>', methods=['GET', 'POST'])
@login_role_required(role=['Admin'])
def address_edit(address_id):
    address = Address.query.filter_by(id=address_id).first_or_404()
    form = AddressEditForm()
    if form.validate_on_submit():
        address.name = form.name.data
        db.session.commit()
        flash('Your changes have been saved.', category='success')
        return redirect(url_for('address'))
    elif request.method == 'GET':
        form.name.data = address.name
    return render_template('address/address_edit.html', title='Edit Location',
                           form=form)


# Show all clients
@app.route('/clients')
@login_required
def client():
    clients = Client.query.order_by('name').all()

    return render_template('client/client.html', title='Clinets',
                           clients=clients)


# client route for each
@app.route('/clients/<int:client_id>')
@login_required
def client_all(client_id):
    client = Client.query.filter_by(id=client_id).first_or_404()
    return render_template('client/client_all.html', title='Clinet',
                           client=client)


# Show form to add client
@app.route('/clients/add', methods=['GET', 'POST'])
@login_role_required(role=['Admin'])
def client_add():
    form = ClientForm()
    form.address_id.choices =\
        [(a.id, a.name) for a in Address.query.order_by('name')]
    if form.validate_on_submit():
        client = Client(name=form.name.data, address_id=form.address_id.data)
        db.session.add(client)
        db.session.commit()
        flash('You added a new client!.', category='success')
        add_record_elastic.delay(form.name.data)
        return redirect(url_for('client'))
    return render_template('client/client_add.html', title='Add Clinet',
                           form=form)


# Show form to edit client
@app.route('/clients/<int:client_id>/edit', methods=['GET', 'POST'])
@login_role_required(role=['Admin'])
def client_edit(client_id):
    client = Client.query.filter_by(id=client_id).first_or_404()
    form = ClientEditForm()
    form.address_id.choices =\
        [(a.id, a.name) for a in Address.query.order_by('name')]
    if form.validate_on_submit():
        client.name = form.name.data
        client.address_id = form.address_id.data
        db.session.commit()
        flash('Your changes have been saved.', category='success')
        update_record_elastic(client_id, form.name.data)
        return redirect(url_for('client'))
    elif request.method == 'GET':
        form.name.data = client.name
        form.address_id.data = client.address_id
    return render_template('client/client_edit.html', title='Edit Clinet',
                           form=form)


# Sales route to show recent all sales
@app.route('/sales')
@login_required
def sales_recent():
    sales = Record.query.filter_by(type_='Sale')\
                  .order_by(desc('date_transaction')).all()
    return render_template('sales/sales.html', title='All Sale', sales=sales)


# Show add form for sale
@app.route('/sale/add', methods=['GET', 'POST'])
@login_role_required(role=['Admin', 'Accountant'])
def sale_add():
    form = RecordForm()
    form.type_.data = 'Sale'
    if form.validate_on_submit():
        client = Client.query.filter_by(name=form.client_name.data).first()
        datetime_object = datetime.datetime.combine(form.date_transaction.data,
                                                    datetime.time.min)
        record = Record(client_id=client.id, amount=form.amount.data)
        record.type_ = form.type_.data
        record.date_transaction = form.date_transaction.data
        db.session.add(record)
        db.session.commit()
        flash('Sale data is inserted.', category='success')
        return redirect(url_for('sales_recent'))
    return render_template('sales/sales_add.html', title='Add Sale', form=form)


# Show edit form for sale
@app.route('/sale/edit/<int:record_id>', methods=['GET', 'POST'])
@login_role_required(role=['Admin', 'Accountant'])
def sale_edit(record_id):
    next_ = request.args.get('next')

    if next_ == "sales":
        return redirect(url_for('sales_recent'))
    else:
        if next_ == "client":
            return redirect(url_for('client_all',
                                    client_id=int(request.args.get('id'))))
    return ""


# Delete the sale reocrd
@app.route('/sale/delete/<int:record_id>', methods=['POST'])
@login_role_required(role=['Admin', 'Accountant'])
def sale_delete(record_id):
    Record.query.filter_by(id=record_id, type_="Sale").delete()
    db.session.commit()
    flash('A sale record is successfully deleted.', 'success')
    next_ = request.args.get('next')
    if next_ == "sales":
        return redirect(url_for('sales_recent'))
    else:
        if next_ == "client":
            return redirect(url_for('client_all',
                                    client_id=int(request.args.get('id'))))
    return abort(404)


# Show recent deposits
@app.route('/deposits')
@login_required
def deposits_recent():
    deposits = Record.query.filter_by(type_='Deposit')\
                     .order_by(desc('date_transaction')).all()
    return render_template('deposit/deposit.html', title='All Deposit',
                           deposits=deposits)


# Show add form for deposit
@app.route('/deposit/add', methods=['GET', 'POST'])
@login_role_required(role=['Admin', 'Accountant'])
def deposit_add():
    form = RecordForm()
    form.type_.data = 'Deposit'
    if form.validate_on_submit():
        client = Client.query.filter_by(name=form.client_name.data).first()
        datetime_object = datetime.datetime.combine(form.date_transaction.data,
                                                    datetime.time.min)
        record = Record(client_id=client.id, amount=form.amount.data)
        record.type_ = form.type_.data
        record.date_transaction = form.date_transaction.data
        db.session.add(record)
        db.session.commit()
        flash('Deposit data is inserted.', category='success')
        return redirect(url_for('deposits_recent'))
    return render_template('deposit/deposit_add.html', title='Add Deposit',
                           form=form)


# Show edit form for deposit
@app.route('/deposit/edit/<int:record_id>', methods=['GET', 'POST'])
@login_role_required(role=['Admin', 'Accountant'])
def deposit_edit(record_id):
    next_ = request.args.get('next')

    if next_ == "deposits":
        return redirect(url_for('deposits_recent'))
    else:
        if next_ == "client":
            return redirect(url_for('client_all',
                                    client_id=int(request.args.get('id'))))
    return ""


# Delete the record of deposit
@app.route('/deposit/delete/<int:record_id>', methods=['POST'])
@login_role_required(role=['Admin', 'Accountant'])
def deposit_delete(record_id):
    Record.query.filter_by(id=record_id, type_="Deposit").delete()
    db.session.commit()
    flash('A deposit record is successfully deleted.', category='success')
    next_ = request.args.get('next')

    if next_ == "deposits":
        return redirect(url_for('deposits_recent'))
    else:
        if next_ == "client":
            return redirect(url_for('client_all',
                                    client_id=int(request.args.get('id'))))
    return abort(404)


# Enter the matched clients name with the given query
@app.route('/search/clients', methods=['GET'])
@login_required
def search_client():
    string = request.args.get('string')
    if string == "" or string is None:
        return ""
    ES_ = connect_elasticsearch()

    datas = search_client_es(ES_, string)
    return json.dumps(datas)


# Call the celery tak which make csv of client
@app.route('/client/<int:client_id>/export', methods=['POST'])
@login_required
def export_client(client_id):
    task = export_client_task.delay(client_id)
    return jsonify({}), 202, {'Location': url_for('taskstatus',
                                                  task_id=task.id)}


# Show client side status of task of given id
@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = export_client_task.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'status': task.info.get('status', '')
        }
        if 'filepath' in task.info:
            response['filepath'] = task.info['filepath']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)
